<?php

namespace Wozedu\DemoBundle\Controller;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('WozeduUserBundle:User', 'u')
            ->join('u.roles', 'r')
            ->where('r.role IN (:username)')
                ->setParameter('username', array('ROLE_USER', 'ROLE_ADMIN'));
       
        //$entities = $em->getRepository('WozeduUserBundle:User')->findBy(array('roles' => 'ROLE_ADMIN'));
        //$entities = $em->getRepository('WozeduUserBundle:User')->findBy(array('roles' => 'ROLE_ADMIN'));

        return $this->render('WozeduDemoBundle:Default:index.html.twig', array(
            'entities' => $qb->getQuery()->getResult(),
        ));
        
        //return $this->render('WozeduDemoBundle:Default:index.html.twig');
    }
}
