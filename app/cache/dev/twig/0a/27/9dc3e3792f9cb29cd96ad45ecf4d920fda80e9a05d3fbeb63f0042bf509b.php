<?php

/* WozeduDemoBundle:Default:index.html.twig */
class __TwigTemplate_0a279dc3e3792f9cb29cd96ad45ecf4d920fda80e9a05d3fbeb63f0042bf509b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f7a2453_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a2453_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/f7a2453_bootstrap_1.css");
            // line 5
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "f7a2453"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a2453") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/f7a2453.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "    <table class=\"table\">
        <tr>
            <td>3234234</td>
            <td>342423</td>
        </tr>
        <tr>
            <td>asdas</td>
            <td>sdasd</td>
        </tr>
        
    </table>
";
    }

    public function getTemplateName()
    {
        return "WozeduDemoBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 11,  61 => 10,  45 => 5,  40 => 4,  37 => 3,  11 => 1,);
    }
}
